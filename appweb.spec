Name:   appweb
Version: 8.2.5
Release: 2
Summary: Appweb Community Edition is a fast embedded web server for securely hosting embedded web management application.
License: GPLv2
URL: https://www.embedthis.com/licensing/index.html
Source0: https://s3.amazonaws.com/embedthis.public/%{name}-%{version}-src.tgz

Patch0: CVE-2021-33254.patch

BuildRequires:  ncurses-devel, asciidoc

%description
Appweb Community Edition is a fast embedded web server for securely hosting embedded web management applications.

%prep
%autosetup -p1 -n %{name}-%{version}
%define debug_package %{nil}


%build
%configure
CFLAGS="$RPM_OPT_FLAGS -DVERSION=appweb-%{version}-%{release}"
%{__make} %{_smp_mflags} --no-print-directory -f projects/appweb-linux-default.mk all

%install
%define ARCH $(uname -m | sed 's/i.86/x86/;s/x86_64/x64/;s/arm.*/arm/;s/mips.*/mips/')
%{__make} --no-print-directory -f projects/appweb-linux-default.mk install
rm -rf ./build/linux-%{ARCH}-default/obj
find ./build/linux-%{ARCH}-default/ -name "*.a" | xargs rm
cp -r ./build/linux-%{ARCH}-default/usr/local/* %{buildroot}

%pre
%preun
%post
%postun

%check

%files
/bin/*
/include/*
/lib/*
/share/*

%changelog
* Tue Jul 12 2022 yanghui <yanghui@kylinos.cn> - 8.2.5-2
- fix CVE-2021-33254

* Thu Nov 18 2021  StrongTiger_001 <StrongTiger_001@isrc.iscas.ac.cn> - 8.2.5-1 
- Update the package to 8.2.5

* Mon Nov 23 2020 seuzw <930zhaowei@163.com> - 8.2.1-2
- fix rpm package no file problem

* Mon Nov 23 2020 StrongTiger_001 <StrongTiger_001@isrc.iscas.ac.cn> - 8.2.1-1
- Bump version to 8.2.1

* Sun Aug 23 2020 StrongTiger_001 <StrongTiger_001@isrc.iscas.ac.cn> - 8.2.1-0
- Package init

